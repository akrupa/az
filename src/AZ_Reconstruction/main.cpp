#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <set>
#include <iterator>
#include <cstring>

using namespace std;

vector<vector<int> > graphMatrix;
vector<bool> used;
vector<set<int> > reconstructedGraph;
vector<vector<int> > reconstructionSteps;

void reconstruct(int from, int to, int dist, vector<int> leaves, int distFromToFirst);
void generateOutputFiles();
vector<vector<int> > split(int u, int v, vector<int> leaves, int distUToFirst);

vector<vector<bool> > resultTree;

int vertNew;
bool output1 = false;
bool output2 = false;

int main(int argc, const char * argv[])
{
    const char *filename;
    if (argc < 2) {
        filename = (char*)"sampleAdv.graph";
        output1 = true;
    } else if(argc < 3) {
        filename = argv[1];
        output1 = true;
    } else {
        filename = argv[1];
        if(strcmp(argv[2],"1") == 0 || strcmp(argv[2],"3") == 0 )
            output1 = true;
        if(strcmp(argv[2],"2") == 0 || strcmp(argv[2],"3") == 0 )
            output2 = true;
    }
    string graphFile(filename);
    
    ifstream file(graphFile);
    
    cout << "Loading " << graphFile << endl;
    if (!file.is_open()) {
        cout << "Error: cannot open file: " << graphFile << endl;
        return -1;
    }
    
    //parse graph file
    
    int leaves;
    file >> leaves;
    
    graphMatrix.resize(leaves);
    resultTree.resize(leaves);
    used.resize(leaves);
    reconstructedGraph.resize(leaves);
    for (int i=0; i < leaves; i++) {
        graphMatrix[i].resize(leaves);
        graphMatrix[i][i] = 0;
    }
    
    int node;
    for (int i=0; i < leaves-1; i++) {
        for (int j=0; j<leaves-i-1; j++) {
            file >> node;
            graphMatrix[i][j+1+i] = node;
            graphMatrix[j+1+i][i] = node;
        }
    }
    
    vector<int> allLeaves;
    for (int i=0; i<leaves; i++) {
        allLeaves.push_back(i);
    }
    if (leaves > 1) {
        used[0] = true;
        vertNew = leaves-1;
        reconstruct(0, 1, graphMatrix[0][1], allLeaves, 0);
    }

    generateOutputFiles();
    return 0;
}

void reconstruct(int from, int to, int dist, vector<int> leaves, int distFromToFirst)
{
    if (used[to]) {
        return;
    }
    used[to] = true;
    int vertOld = from;
    vector<vector<int> > splitResult = split(from, to, leaves, distFromToFirst);
    
    for (int i=0; i<splitResult[0].size(); i++) {
        reconstruct(from, splitResult[0][i], graphMatrix[to][splitResult[0][i]], splitResult[0], distFromToFirst);
    }
    
    if (splitResult.size() == 1) {
        reconstructedGraph[from].insert(to);
        reconstructedGraph[to].insert(from);
        vector<int> v;
        v.push_back(from);
        v.push_back(to);
        reconstructionSteps.push_back(v);
    }
    
    for (int i=1; i<splitResult.size(); i++) {
        vertNew++;
        
        set<int> newVec;
        reconstructedGraph.push_back(newVec);
        reconstructedGraph[vertOld].insert(vertNew);
        reconstructedGraph[vertNew].insert(vertOld);
        vector<int> v;
        v.push_back(vertOld);
        v.push_back(vertNew);
        reconstructionSteps.push_back(v);
        
        if (i == splitResult.size() -1) {
            reconstructedGraph[vertNew].insert(to);
            reconstructedGraph[to].insert(vertNew);
            vector<int> v;
            v.push_back(vertNew);
            v.push_back(to);
            reconstructionSteps.push_back(v);
        }
        distFromToFirst++;
        vertOld = vertNew;
        if (!splitResult[i].empty()) {
            reconstruct(vertNew, splitResult[i][0], graphMatrix[to][splitResult[i][0]], splitResult[i], distFromToFirst);
        }
    }
}

vector<vector<int> > split(int u, int v, vector<int> leaves, int distUToFirst)
{
    vector<vector<int> > result(graphMatrix[0][v] - distUToFirst);
    if (result.empty()) {
        return result;
    }
    
    for (int i=0; i < leaves.size(); i++) {
        int leave = leaves[i];
        if (!used[leave]) {
            int d_uv = graphMatrix[0][v] - distUToFirst;
            int d_uw = graphMatrix[0][leave] - distUToFirst;
            int d_ux = (d_uv + d_uw - graphMatrix[v][leave])/2;
            result[d_ux].push_back(leave);
        }
    }
    
    return result;
}

void generateOutputFiles() {
    
    if (output2) {
        ofstream myfile;
        myfile.open ("output2.txt");
        int i = 0;
        for (auto it = reconstructedGraph.begin(); it != reconstructedGraph.end(); it++) {
            myfile << i++ << " " << it->size() << " ";
            for (auto iit = it->begin(); iit != it->end(); iit++) {
                myfile << *iit << " ";
            }
            myfile << endl;
        }
        myfile.close();
    }
    
    printf("Graph structure:\n");
    int i = 0;
    for (auto it = reconstructedGraph.begin(); it != reconstructedGraph.end(); it++) {
        printf("%d: ", i++);
        for (auto iit = it->begin(); iit != it->end(); iit++) {
            printf("%d ", *iit);
        }
        printf("\n");
    }
    
    
    
    if (output1) {
        ofstream myfile;
        myfile.open ("output1.txt");
        myfile << reconstructedGraph.size() << " " << reconstructionSteps.size() << endl;
        for (auto it = reconstructionSteps.begin(); it != reconstructionSteps.end(); it++) {
            myfile << (*it)[0] << " " << (*it)[1] << endl;
        }
        myfile.close();
    }
    
    printf("\nGraph reconstruction:\n");
    for (auto it = reconstructionSteps.begin(); it != reconstructionSteps.end(); it++) {
        cout << (*it)[0] << " " << (*it)[1] << endl;
    }
}
